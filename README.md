# Checkpointed SDPA-GMP #

This is a very lightly modified version of SDPA-GMP (http://sdpa.sourceforge.net/) to allow for checkpointing.  In addition to the requirements of SDPA-GMP, the checkpointed version requires the Boost C++ Libraries (http://www.boost.org/).  Run `./configure` as usual, but with the additional argument `--with-boost-libdir=/path/to/boost/libraries`, where the boost libraries should include `libboost_serialization.so` and `libboost_filesystem.so`.

After building the software, run `sdpa_gmp` with no arguments to get usage information.