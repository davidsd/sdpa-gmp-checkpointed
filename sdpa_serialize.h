// Author(s): D. Simmons-Duffin, May 2014

#ifndef SDPA_SERIALIZE_H_
#define SDPA_SERIALIZE_H_

#include "boost/serialization/serialization.hpp"
#include "boost/serialization/base_object.hpp"
#include "boost/serialization/utility.hpp"
#include "boost/serialization/split_free.hpp"
#include "boost/archive/text_oarchive.hpp"
#include <sstream>
#include <gmpxx.h>
#include <sdpa_parts.h>

namespace boost {
  namespace serialization {

    template<class Archive>
    void load(Archive& ar, mpf_class& f, unsigned int version) {
      std::string s;
      ar & s;
      f = mpf_class(s);
    }

    template<class Archive>
    void save(Archive& ar, mpf_class const& f, unsigned int version) {
      std::ostringstream os;
      os.precision(f.get_prec());
      os << f;
      std::string s = os.str();
      ar & s;
    }

    template<class Archive>
    void serialize(Archive& ar, sdpa::StepLength& alpha, unsigned int version) {
      ar & alpha.primal;
      ar & alpha.dual;
    }

    template<class Archive>
    void serialize(Archive& ar, sdpa::DirectionParameter& beta, unsigned int version) {
      ar & beta.value;
    }

    template<class Archive>
    void serialize(Archive& ar, sdpa::Switch& reduction, unsigned int version) {
      ar & reduction.switchType;
    }
    
    template<class Archive>
    void serialize(Archive& ar, sdpa::AverageComplementarity& mu, unsigned int version) {
      ar & mu.initial;
      ar & mu.current;
    }
    
    template<class Archive>
    void serialize(Archive& ar, sdpa::RatioInitResCurrentRes& theta, unsigned int version) {
      ar & theta.primal;
      ar & theta.dual;
    }
    
    template<class Archive>
    void serialize(Archive& ar, sdpa::SolveInfo& solveInfo, unsigned int version) {
      ar & solveInfo.rho;
      ar & solveInfo.etaPrimal;
      ar & solveInfo.etaDual;
      ar & solveInfo.objValPrimal;
      ar & solveInfo.objValDual;
    }
    
    template<class Archive>
    void serialize(Archive& ar, sdpa::Phase& phase, unsigned int version) {
      ar & phase.nDim;
      ar & phase.value;
    }
    
    // This load function only works if mat has been previously
    // initialized with the correct dimensions and array allocations.
    // It does not work with a default constructor!
    template<class Archive>
    void load(Archive& ar, sdpa::DenseLinearSpace& mat, unsigned int version) {
      int SDP_nBlock = mat.SDP_nBlock;
      int LP_nBlock = mat.LP_nBlock;
  
      // for SDP
      for (int l=0; l<SDP_nBlock; ++l) {
        int size = mat.SDP_block[l].nRow;
        for (int i=0; i<size; ++i) {
          for (int j=0; j<size; ++j) {
            mpf_class tmp;
            ar >> tmp;
            if (i<=j && tmp!=0.0) {
              mat.setElement_SDP(l,i,j,tmp);
            }
          }
        }
      }

      // for LP
      for (int j=0; j<LP_nBlock; ++j) {
        mpf_class tmp;
        ar >> tmp;
        if (tmp!=0.0) {
          mat.setElement_LP(j,tmp);
        }
      }
    }

    // This save function does not save all the necessary data to a
    // file.  It will only work with the above load function if
    // there's a alternate way of initializing the DenseLinearSpace.
    template<class Archive>
    void save(Archive& ar, sdpa::DenseLinearSpace const& mat, unsigned int version) {
      int SDP_nBlock = mat.SDP_nBlock;
      int LP_nBlock = mat.LP_nBlock;
  
      // for SDP
      for (int l=0; l<SDP_nBlock; ++l) {
        int size = mat.SDP_block[l].nRow;
        for (int i=0; i<size; ++i) {
          for (int j=0; j<size; ++j) {
            mpf_class tmp = mat.getElement_SDP(l,i,j);
            ar << tmp;
          }
        }
      }

      // for LP
      for (int j=0; j<LP_nBlock; ++j) {
        mpf_class tmp = mat.getElement_LP(j);
        ar << tmp;
      }
    }

    // Assumes space for v has already been allocated -- does not work
    // with default constructor!
    template<class Archive>
    void serialize(Archive& ar, sdpa::Vector& v, unsigned int version) {
      for (int k=0; k<v.nDim; ++k) {
        ar & v.ele[k];
      }
    }

    // Assumes space for currentPt has already been allocated -- does
    // not work with default constructor!
    template<class Archive>
    void serialize(Archive& ar, sdpa::Solutions& currentPt, unsigned int version) {
      ar & currentPt.yVec;
      ar & currentPt.xMat;
      ar & currentPt.zMat;
    }

    template<class Archive>
    void serializeState(Archive& ar, 
                        sdpa::Solutions& currentPt,
                        sdpa::StepLength& alpha,
                        sdpa::DirectionParameter& beta,
                        sdpa::Switch& reduction,
                        sdpa::AverageComplementarity& mu,
                        sdpa::RatioInitResCurrentRes& theta,
                        sdpa::SolveInfo& solveInfo,
                        sdpa::Phase& phase) {
      ar & currentPt;
      ar & alpha;
      ar & beta;
      ar & reduction;
      ar & mu;
      ar & theta;
      ar & solveInfo;
      ar & phase;
    }

  }  // namespace serialization
}  // namespace boost

BOOST_SERIALIZATION_SPLIT_FREE(mpf_class)
BOOST_CLASS_VERSION(mpf_class, 0)
BOOST_SERIALIZATION_SPLIT_FREE(sdpa::DenseLinearSpace)
BOOST_CLASS_VERSION(sdpa::DenseLinearSpace, 0)
BOOST_CLASS_TRACKING(sdpa::StepLength,             boost::serialization::track_never)
BOOST_CLASS_TRACKING(sdpa::DirectionParameter,     boost::serialization::track_never)
BOOST_CLASS_TRACKING(sdpa::Switch,                 boost::serialization::track_never)
BOOST_CLASS_TRACKING(sdpa::AverageComplementarity, boost::serialization::track_never)
BOOST_CLASS_TRACKING(sdpa::RatioInitResCurrentRes, boost::serialization::track_never)
BOOST_CLASS_TRACKING(sdpa::SolveInfo,              boost::serialization::track_never)
BOOST_CLASS_TRACKING(sdpa::Phase,                  boost::serialization::track_never)
BOOST_CLASS_TRACKING(sdpa::DenseLinearSpace,       boost::serialization::track_never)
BOOST_CLASS_TRACKING(sdpa::Vector,                 boost::serialization::track_never)
BOOST_CLASS_TRACKING(sdpa::Solutions,              boost::serialization::track_never)

#endif  // SDPA_SERIALIZE_H_
